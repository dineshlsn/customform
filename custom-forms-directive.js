(function(){
	
'use strict';
	
angular.module('lsnNgModule', []);





/**
 * @ngdoc directive
 * @name dynamicforms.Directives.customFormsDirective
 * @description customFormsDirective directive
 */
angular
    .module('lsnNgModule')
    .directive('customFormsDirective', ['$compile', 'Dynamicservices',
    function ($compile, Dynamicservices) {
            //return {
            // name: '',
            // priority: 1,
            // terminal: true,
            // scope: {}, // {} = isolate, true = child, false/undefined = no change
            // controller: function($scope, $element, $attrs, $transclude) {},
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
            // template: '',
            // templateUrl: '',
            // replace: true,
            // transclude: true,
            // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}}))
            var linker = function ($scope, element, attrs,$rootScope) {

                $scope.answers = {};
                $scope.results = [];
                $scope.selection = [];
                $scope.pagingIndex = 0;

                function pushRecord(tempObj) {
                    $scope.results.push(tempObj);
                }
                /**
                 *Get templates based on category
                 **/

                $scope.getTemplate = function (field,path) {
                    var contentType = field.field_type;
                    var templateUrl = '';
                    switch (contentType) {
                    case 'textfield':
                        templateUrl = path+'/textfield.html';
                        break;
                    case 'email':
                        templateUrl = path+'/email.html';
                        break;
                    case 'phonenumber':
                        templateUrl = path+'/phonenumber.html';
                        break;
                    case 'radio':
                        templateUrl =  path+'/radio.html';
                        break;
                    case 'checkbox':
                        templateUrl =  path+'/checkbox.html';
                        break;
                    case 'textarea':
                        templateUrl =  path+'/textarea.html';
                        break;
                    case 'currency':
                        templateUrl =  path+'/currency.html';
                        break;
                    case 'date':
                        templateUrl =  path+'/date.html';
                        break;
                    case 'file':
                        templateUrl =  path+'/file.html';
                        break;
                    case 'rating':
                        templateUrl =  path+'/rating.html';
                        break;
                    case 'time':
                        templateUrl =  path+'/time.html';
                        break;
                    case 'url':
                        templateUrl =  path+'/url.html';
                        break;
                    case 'dropdown':
                        templateUrl =  path+'/dropdown.html';
                        break;
                    case 'duration':
                        templateUrl =  path+'/duration.html';
                        break;
                    case 'password':
                        templateUrl =  path+'/password.html';
                        break;
                    case 'readOnly':
                        templateUrl =  path+'/readOnly.html';
                        break;
                     case 'dependentDropdown':
                        templateUrl =  path+'/dependentDropdown.html';
                        break;
                    }
                    return templateUrl;
                };
                $scope.getList = function (data) {
                    //console.log(data)
                    $scope.dependentCallback({
                        data: angular.copy(data)
                     });
                    console.log(data)
                    //$rootScope.val =data;
                    //console.log($rootScope.val)
        $scope.states=Dynamicservices.getDependentDropdown(data);

                  console.log($scope.states)
  }
               //Method for go to next step

                $scope.nextStep = function () {
                    $scope.pagingIndex++;
                };

                //Method for go to previous step

                $scope.prevStep = function () {
                    $scope.pagingIndex--;
                };


                /**
                code for checkbox result object
                **/
                $scope.getOptions = function (fieldId, optionId, optionText, type) {
                    var tempObj = {

                        'field_id': fieldId,
                        'option_id': [optionId],
                        'option_text': optionText
                    };
                    var idx = $scope.selection.indexOf(optionText);
                    // is currently selected, need to deselect and pop the element from array

                    if (idx > -1) {
                        $scope.selection.splice(idx, 1);

               /**

                * 1. Find field_id  from results array
                * 2. find element in array and remove it from array
                **/

                        var isExists = _.where($scope.results, {
                            'field_id': tempObj.field_id
                        });

                        var value = _.first(isExists);
                        value['option_id'].splice(idx, 1);
                        console.log($scope.results);
                    } else {

                        // is newly selected

                        $scope.selection.push(optionText);

                    /**
                     * 1. Find field_id  in results array
                     * 2.
                     *   a. if(field_id){} Answering the same field when                                             selecting another option
                                1.  Push option id to Option_id array on field_id
                         b. else { new field first time answering: Push field_id                                    obj to results array}
                     **/
                        var isExists = _.where($scope.results, {
                            'field_id': tempObj.field_id
                        });

                        var value = _.first(isExists);

                        if (_.isUndefined(value)) {
                            pushRecord(tempObj);
                        } else {
                            value['option_id'].push(tempObj.option_id[0]);
                        }
                        $scope.answers[tempObj.field_id] = tempObj.option_text;
                        console.log($scope.results);
                    }

                };
                /**
                     * 1. Find field_id in results array
                     * 2.
                     *   a. if(field_id){} Answering the same field when                                             modify the answer
                                1.  Push field value to result array on field Id
                         b. else { new field first time answering: Push field_id                                     to results array}
                     **/

                $scope.setUserChoice = function (fieldId, optionId, fieldValue, type) {

                    var tempObj = {

                        'field_id': fieldId,
                        'option_id': optionId,
                        'field_value': fieldValue
                    };

                    var isExists = _.where($scope.results, {
                        'field_id': tempObj.field_id

                    });

                    var value = _.first(isExists);
                    if (_.isUndefined(value)) {
                        pushRecord(tempObj);
                    } else {

                        value.field_id = tempObj.field_id;
                        value.field_value = tempObj.field_value;
                    }
                    $scope.answers[tempObj.field_id] = tempObj.field_value;
                    console.log($scope.results);
                };

                $scope.submitResult = function (data) {
                    $scope.dynamicCallback({
                        data: angular.copy($scope.results)
                    });
                };
                $scope.cancel = function () {
                    window.history.back();
                };

            };

            return {
                restrict: 'E',
                link: linker,

                scope: {
                    formFields: '=data',
                    dynamicCallback: '&',
                    dependentCallback: '&',
                    path: '=url'

                      },
                controller: function ($scope, Dynamicservices) {

                },

                template: '<center>' + '<form name="myForm" novalidate="novalidate" class="form-horizontal">' + '<div class="form-group">' + '<div ng-repeat= "form in formFields">' + '<div><h4>{{form.form_title}}</h4></div>' + '<div><h4>{{form.form_description}}</h4></div>' + '<div ng-repeat="field in form.fields | orderBy:&quot;priority&quot;">' + '<div ng-include="getTemplate(field,path)"></div>' + '</div>' + '</div>' + '<div ng-repeat= "form in formFields">' + '<div ng-repeat="group in form.group">' + '<div ng-if="pagingIndex == $index">' + '<legend align="left">{{group.name}}</legend>' + '<div ng-repeat="field in group.fields">' + '<div ng-include="getTemplate(field,path)"></div>' + '</div>' + '<button class="btn btn-primary" ng-click="prevStep()" ng-show="!$first">Previous</button>' + '<button class="btn btn-primary" ng-click="nextStep()" ng-show="!$last">Next</button>' + '</div>' + '</div>' + '</div>' + '<button class="btn btn-primary" style="float:right" ng-click="cancel()">Cancel</button>' + '<button style="float:right" ng-disabled="myForm.$invalid" type="submit" class="btn btn-primary" ng-click="submitResult(data)">Submit</button>' + '</div>' + '</center>'
            };
    }
  ]);
})();
